# -*- coding: UTF8 -*-
# 
# 代码主要参考：
# http://www.tuicool.com/articles/Qjei2e
# pywin32下载地址https://sourceforge.net/projects/pywin32/files/pywin32/Build%20214/
#
# https://sourceforge.net/projects/pywin32/files/pywin32/Build%20214/pywin32-214.win32-py2.7.exe/download
# http://www.sharejs.com/codes/python/6389
# http://www.cnblogs.com/samren/archive/2012/09/12/2682501.html

import win32serviceutil 
import win32service 
import win32event
import winerror
import servicemanager
import sys
import gl
from common import *

class PythonService(win32serviceutil.ServiceFramework): 

    _svc_name_ = "A90000"   #  服务名
    _svc_display_name_ = "A90000"   # 服务在windows系统中显示的名称
    _svc_description_ = "This is My Rpc Server Service5"   #服务的描述

    def __init__(self, args): 
        win32serviceutil.ServiceFramework.__init__(self, args) 
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        self.run = True
        
    def SvcDoRun(self):
		self.doWork()
                
    def SvcStop(self): 
        # 服务已经停止
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING) 
        win32event.SetEvent(self.hWaitStop)
		
		# 此处用于结束rpc server
		# 不知为何，在MyDaemon中重写stopWork方法，不起作用
        logging.debug('rpc service stop...')
        gl.GLOBALCTL = False
        clisock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        clisock.connect(ADDR)
        # self.stopWork()

    def doWork(self):
		pass
		
    def stopWork(self):
		pass
		
if __name__=='__main__':
    if len(sys.argv) == 1:
        try:
            evtsrc_dll = os.path.abspath(servicemanager.__file__)
            servicemanager.PrepareToHostSingle(PythonService)
            servicemanager.Initialize('PythonService', evtsrc_dll)
            servicemanager.StartServiceCtrlDispatcher()
        except win32service.error, details:
            if details[0] == winerror.ERROR_FAILED_SERVICE_CONTROLLER_CONNECT:
                win32serviceutil.usage()
    else:
        win32serviceutil.HandleCommandLine(PythonService)
