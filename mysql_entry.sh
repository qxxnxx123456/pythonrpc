#!/bin/bash

if [ ! -f /var/lib/mysql/ibdata1 ]; then
    mysql_install_db
fi
/etc/init.d/mysql start
echo "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('123456');\n" | mysql 
echo "GRANT ALL ON *.* TO root@'%' IDENTIFIED BY '123456' WITH GRANT OPTION; FLUSH PRIVILEGES;\n" | mysql -uroot -p123456
tail -f /var/log/mysql/error.log
