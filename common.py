# -*- coding: utf-8 -*-
import json
import sys, os, socket, datetime
import struct
import threading
import Queue
from time import ctime, time, sleep
from random import randint
import gl

WIN32 = False
if sys.platform == 'win32':
    WIN32 = True

HOST = '127.0.0.1'
PORT = 31601
ADDR = (HOST, PORT)

HEADER_FORMAT = '>BH'
HEADER_LENGTH = struct.calcsize(HEADER_FORMAT)

# 线程等待一定时间后退出
TIME_OUT = 3600
# 线程数
THREAD_INIT = 2
# 线程处理一定量任务后退出
THREAD_MAX_TASK = 100

import logging

if WIN32:
    # ???程序作为服务启动后，日志文件位于C:\WINDOWS\system32
    fn = 'myrpc_' + datetime.date.today().strftime('%Y%m') + '.log'
else:
    # 日志位于/var/log/myrpc_[month].log
    fn = '/var/log/' + 'myrpc_' + datetime.date.today().strftime('%Y%m') + '.log'
logging.basicConfig(level=logging.DEBUG,
                format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                datefmt='%a, %d %b %Y %H:%M:%S',
                filename=fn,
                filemode='a')
    
# logging.debug('This is debug message')
# logging.info('This is info message')
# logging.warning('This is warning message')

class MyrpcError(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return self.msg

def recvall(sock, length, tmo=5):
    """
    read all n bytes of data from sock
    如果等待tmo时间后仍无响应，则抛出异常MyrpcError
    """
    data = ''
    st = time()
    while (len(data) < length) and (time() - st < tmo):
        try:
            more = sock.recv(length - len(data))
            if more:
                data += more
        except:
            pass

    if len(data) == length:
        return data
    else:
        logging.debug('socket %s timeout' % sock)
        raise MyrpcError('recv timeout, %d bytes of %d bytes received' % (len(data), length))

def closesock(s):
    """
    Abandon data and close
    """
    st = time()
    while time() - st < 1:
        try:
            more = s.recv(1)
        except:
            pass
    s.close()
    logging.debug('socket %s closed' % s)

def sendall(fd, data):
    total = len(data)
    offset = 0
    while offset < total:
        try:
            n = fd.send(data[offset:])
        except socket.error as e:
            raise MyrpcError('Failed to write to socket: %s' % e)

        if n <= 0:
            raise MyrpcError('Failed to write to socket')
        else:
            offset += n

def parse_header(buf):
    try:
        ver, length = struct.unpack(HEADER_FORMAT, buf)
    except struct.error, e:
        raise MyrpcError('error when unpack packet header: %s' % e)
    
    return {'ver': ver, 'length': length}

def parse_body(buf):
    try:
        ret = json.loads(buf)
    except Exception, e:
        raise MyrpcError('bad body str: ' + str(e))
    return ret

class MiThread(threading.Thread):
    """使用多线程处理请求"""
    def __init__(self, func, name=None, pool = None):
        super(MiThread, self).__init__()
        self.workQueue = Queue.Queue()
        self.func = func
        self.name = func.__name__ if not name else name
        self.timeout = TIME_OUT
        self.busy = False
        self.count = 0
        self.pool = pool
        self.start()

    def cur_task_num(self):
        self.workQueue.qsize()

    def do_task(self, args):
        self.count += 1
        self.workQueue.put(args)
        self.busy = True
        logging.debug('Thread %s task added:%s' % (self.ident, args[0]))
    
    def run(self):
        logging.debug('Thread %d start at %s' % (self.ident, ctime()))
        start = time()
        while time() - start < self.timeout:
            if self.pool and self.pool.run == False:
                logging.debug('Thread %d: pool tells me to STOP' % self.ident)
                break
            try:
                my_args = self.workQueue.get(False)
                self.busy = True
                self.func(*my_args)
                self.busy = False
                logging.debug('Thread %s task done:%s' % (self.ident, my_args[0]))
                if self.count == THREAD_MAX_TASK:                    
                    logging.debug('Thread %s exited due to MAX tasks done.' % self.ident)
                    break
                start = time()
            except Queue.Empty:
                pass
            sleep(0.1)
                #logging.debug('Thread %s exited due to Queue.Empty, %d tasks done.' % (self.ident, self.count))
                #break

class MiThreadPool(object):
    """线程池"""
    def __init__(self, func, name=None, pool_size=THREAD_INIT):
        self.func = func
        self.name = func.__name__ if not name else name
        self.pool_size = pool_size
        self._pool = []
        self.run = True
        self._create_thread_pool()

    def _create_thread(self):
        athread = MiThread(self.func, self.name, self)
        return athread

    def _create_thread_pool(self):
        for i in range(self.pool_size):
            self._pool.append(self._create_thread())

    def _get_thread(self):
        new_thread = None
        bingo = randint(0, self.pool_size-1)
        min_task_num = self._pool[bingo].cur_task_num()
        for i in range(self.pool_size):
            if not self._pool[i].isAlive():
                logging.debug('Thread %d not Alived' % self._pool[i].ident)
                new_thread = self._create_thread()
                self._pool[i] = new_thread
            else:
                task_size = self._pool[i].cur_task_num()
                if task_size < min_task_num:
                    min_task_num = task_size
                    bingo = i
                elif (task_size == min_task_num) and (not self._pool[i].busy):
                    bingo = i                    
        
        if new_thread:
            return new_thread
        else:
            return self._pool[bingo]

    def add_task(self, args):
        self._get_thread().do_task(args)

    def destroy(self):
        self.run = False
        while True:
            alive = False
            for i in range(self.pool_size):
                alive = alive or self._pool[i].isAlive()
            if not alive:
                break
            sleep(0.1)
