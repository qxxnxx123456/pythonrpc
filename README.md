### 使用：
+ 运行win_rpc_server.py文件，启动rpc server。
+ 运行node_client.js或rpc_client.py，相当于客户端，调用rpc server提供的函数，并获取调用结果。
### 传输格式：
+ 传输格式，请求头+请求体，具体参见客户端代码：
+ 请求头：版本号+请求体长度。
  （>BH格式，即大端序，一个byte长度，一个unsighed short长度的数据，参考http://www.cnblogs.com/gala/archive/2011/09/22/2184801.html）
+ 请求体：服务名+调用信息。
  （json格式：{ 'service': 'my_rpc_demo', 'call': ['str_len', 'hello rpc'] } ）
