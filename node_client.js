/* RPC server的nodejs客户端
 * 
 *
 *
 */


var net = require('net');

var HOST = '127.0.0.1';
var PORT = 31601;

var client = new net.Socket();
client.connect(PORT, HOST, function() {

    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
    // 建立连接后立即向服务器发送数据，服务器将收到这些数据
    
	// 请求体
    var bodyobj = {
	    'service': 'my_rpc_demo',
	    'call': ['str_len', 'hello rpc'],
	}
	var bodystr = JSON.stringify(bodyobj)
	var body_len = bodystr.length

	// 这里header的构造是为了满足 python struct 的>BH数据格式（header被定义为此格式）
	// TBD: nodejs是否有类似python struct的模块？
	
	// 请求头
	var header = new Buffer(3);
    header[0]=1;
    header[1]=body_len>>8;
    header[2]=body_len;
	// req-1 正常请求：
    client.write(header); //发送请求头
    client.write(bodystr); //发送请求体
	
	// req-2 正常请求：
    client.write(header);
    client.write(bodystr);
	
	// req-3 结束请求：
    header[1]=header[2]=0;
    client.write(header);
    client.write(bodystr);

});

// 为客户端添加“data”事件处理函数
// data是服务器发回的数据

// D中保存所有响应数据
var D = ''
client.on('data', function(data) {
	data = data.toString()
	D += data
});

// 为客户端添加“close”事件处理函数
client.on('close', function() {
    console.log('Connection closed');
    var data = D
    var start, ver, len, step = 0

	// 解析响应数据
    for (start=0;start<data.length;){
    	//console.log('S:'+start)
	    ver = data[start++].charCodeAt()
	    len = (data[start++].charCodeAt()<<8)+data[start++].charCodeAt()
	    console.log('ver: ' + ver + ';len: ' + len);

	    ret = JSON.parse(data.substring(start, start+len))
	    console.log('Return:' + ret.ret)
	    start += len
	}
});