# -*- coding: utf-8 -*-
#
# rpc server主程序，支持windows和linux下运行
# windows下作为服务运行，linux下作为守护进程运行
# client在使用时，遵循“请求头+请求体的使用方式”：
# 	具体可参见rpc_client.py的python实现或node_client.js的nodejs实现
#
#
# windwos下使用方式：
# 	安装服务：python win_rpc_server.py install
#	使用服务：打开windows服务管理进行启动、停止、重启等
#	移除服务：python win_rpc_server.py stop
#
# linux下使用方式：
#	运行：python win_rpc_server.py start
#   停止：python win_rpc_server.py stop

import os, sys
import socket
import json
from common import *
from time import sleep
import gl
from rpc_service import str_len

# def stopWork():
    # logging.debug('rpc service stop...')
    # gl.GLOBALCTL = False
    # clisock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # clisock.connect(ADDR)

# import threading
# gl.timer = threading.Timer(15, stopWork)
# timer.start()

if WIN32:
	import win32serviceutil 
	import win32service 
	import win32event
	import winerror
	import servicemanager
	from winService import PythonService as BackEndSvc
else: # linux2
	from deamonize import Daemon as BackEndSvc

class MyrpcService(object):
    def __init__(self, name):
        self.name = name
        self.func_table = {}

class MyrpcServer(object):
    def __init__(self):
        self.services = {}

    def create_service(self, svcname):
        service = MyrpcService(svcname)
        self.services[svcname] = service

    def register_function(self, svcname, fn, fname=None):
        service = self.services[svcname]
        if fname == None:
            fname = fn.__name__
        service.func_table[fname] = fn

    def _call_function(self, svcname, fcallstr):
        """input str -> output str"""
        try:
            argv = json.loads(fcallstr)
        except Exception, e:
            raise MyrpcError('bad call str: ' + str(e))

        service = self.services[svcname]

        fname = argv[0]
        fn = service.func_table.get(fname, None)
        if fn is None:
            raise MyrpcError('No such funtion %s' % fname)

        ret = fn(*argv[1:])
        return ret

    def call_function(self, svcname, fcallstr):
        try:
            retVal = self._call_function(svcname, fcallstr)
        except Exception, e:
            ret = {'err_code': 555, 'err_msg': str(e)}
        else:
            ret = {'ret': retVal}

        return json.dumps(ret)

def worker(rpc_server):
    def my_worker(clisock):
        while gl.GLOBALCTL:
            try:
                logging.debug('start recvall head from %s', clisock)
                cli_header_buf = recvall(clisock, HEADER_LENGTH)

                cli_header = parse_header(cli_header_buf)
                logging.debug('header received: %s' % json.dumps(cli_header))

                if cli_header['length'] == 0:# 客户端要求结束
                    ret = json.dumps({'ret':'closed'})
                    sendall(clisock, struct.pack(HEADER_FORMAT, 0, len(ret)))
                    sendall(clisock, ret)
                    break
 
                logging.debug('start recvall body from %s', clisock)
                cli_body_buf = recvall(clisock, cli_header['length'])
                logging.debug('body received: %s' % cli_body_buf)

                cli_body = parse_body(cli_body_buf)

                ret = rpc_server.call_function(cli_body['service'], json.dumps(cli_body['call']))
                resp_header = struct.pack(HEADER_FORMAT, 1, len(ret))
                sendall(clisock, resp_header)
                sendall(clisock, ret)
            except MyrpcError, e:
                logging.debug('Error: %s in socket %s' % (str(e), clisock))
                break
        closesock(clisock)
        # if gl.flag:
            # gl.timer.start()
            # gl.flag = False
    return my_worker

def start_rpc_server(rpc_server, worker):
    try:
        servsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        servsock.bind(ADDR)
        servsock.listen(1)
    except Exception,e:
        logging.debug('Server socket create Faild')
        exit(1)

    # 创建线程池
    thread_pool = MiThreadPool(worker(rpc_server))

    while gl.GLOBALCTL:
        logging.debug('waiting for connection...')
        try:
            clisock, addr = servsock.accept()
        except KeyboardInterrupt:
            logging.debug('KeyboardInterrupt...')
            break
        # 设为非阻塞式
        clisock.setblocking(0)
        logging.debug('...connected from:%s' % clisock)		
		
        if not gl.GLOBALCTL:
            closesock(clisock)
            break

        # 将请求分发至线程池处理
        thread_pool.add_task((clisock,))

    # 退出线程池
    thread_pool.destroy()

    return servsock

def start_server():
    searpc_server = MyrpcServer()
    searpc_server.create_service('my_rpc_demo')

    # 将str_len注册到rpc server中以待调用
    searpc_server.register_function('my_rpc_demo', str_len, 'str_len')
    start_rpc_server(searpc_server, worker)

class MyDaemon(BackEndSvc):
    def doWork(self):
    	logging.debug('rpc service start...')
        start_server()
		
	# 此函数暂无用。参见winService.py中的SvcStop()
	def stopWork(self):
		logging.debug('rpc service stop...')
		gl.GLOBALCTL = False
		clisock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		clisock.connect(ADDR)

if __name__ == '__main__':
    logging.debug('main started')
    if WIN32:
	    logging.debug('On win32 start...')
	    if len(sys.argv) == 1:
	        try:
	            evtsrc_dll = os.path.abspath(servicemanager.__file__)
	            servicemanager.PrepareToHostSingle(MyDaemon)
	            servicemanager.Initialize('MyDaemon', evtsrc_dll)
	            servicemanager.StartServiceCtrlDispatcher()
	        except win32service.error, details:
	            if details[0] == winerror.ERROR_FAILED_SERVICE_CONTROLLER_CONNECT:
	                win32serviceutil.usage()
	    else:
	        win32serviceutil.HandleCommandLine(MyDaemon)
    else:
        logging.debug('On linux2 start...')
        if len(sys.argv) >= 2:        
            daemon = MyDaemon("/var/run/rpc_server.pid")
            daemon.control(sys.argv[1])
        else:
            print "usage: %s start|stop|restart|status" % sys.argv[0]
