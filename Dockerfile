FROM ubuntu:14.04
MAINTAINER Qian < qianxi@iscas.ac.cn>

RUN rm -rf /etc/apt/sources.list.d/*
RUN echo "deb http://ubuntu.cn99.com/ubuntu trusty main universe" > /etc/apt/sources.list
RUN groupadd -r mysql && useradd -r -g mysql mysql

RUN apt-get update && apt-get -y install curl --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get -y install mysql-client mysql-server --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0\ncharacter-set-server = utf8/" /etc/mysql/my.cnf

ADD ./mysql_entry.sh /opt/mysql_entry.sh
EXPOSE 3306
CMD ["/bin/bash", "/opt/mysql_entry.sh"]
