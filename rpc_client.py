# -*- coding: utf-8 -*-
# nodejs client参考https://cnodejs.org/topic/4fb1c1fd1975fe1e1310490b
import json
import socket
import struct
from time import sleep
from common import *

# 调用远程函数
def call_remote_func(s, req_header, req_body):
    """
    called by searpc_func to send the request and receive the result
    """
    # send the header
    s.sendall(req_header)
    print 'header sended'
    # send the JSON data
    s.sendall(req_body)
    print 'body sended'

    # read the returned header
    resp_header_buf = recvall(s, HEADER_LENGTH, 10)
    #parse the header
    resp_header = parse_header(resp_header_buf)
    # print 'header received: %s' % json.dumps(resp_header)
    resp_body_buf = recvall(s, resp_header['length'], 10)
    # print 'body received: %s' % resp_body_buf
    resp_body = parse_body(resp_body_buf)

    return resp_body

if __name__ == '__main__':  
	req = {} # 请求由请求头和请求体构成

    # 请求体的格式
	req['body'] = {
	    'service': 'my_rpc_demo', # 服务名
	    'call': ['str_len', 'hello rpc'], # 函数名，参数1，参数2，....
	}
	req_body = json.dumps(req['body'])
	
    i = 0
	# 请求头的格式
	req['header'] = {
		'ver': 2, # ver是版本
		'length': len(req_body) if i==0 else 0 # length是请求体的长度（若为0，则标记为退出）
	}
	req_header = struct.pack(HEADER_FORMAT, req['header']['ver'], req['header']['length'])

	# 连接server
	clisock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	clisock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	clisock.connect(ADDR)

	#执行调用
	ret = call_remote_func(clisock, req_header, req_body)

	if ret.has_key('ret'):
		print 'Response: ', ret.get('ret')
	else:
		print 'Error: ', ret.get('err_msg')
